import matplotlib.pyplot as plt
import numpy as np
import math as mt


#fouction that generate random data (distributions) that would be centered in a point
def randomDataGenerator(mu, sigma, n_points, centers):
    '''
    this function generate a ditribution of points centered in a point.

    randomDataGenerator(mu, sigma, n_points, centers)

    mu      : the mean of your distribution         (type : float or int)
    sigma   : the covariance of your distribution   (type : float or int)
    n_points: number of points in you distribution  (typr : int)
    centers : point where you distribution is centered (type : list : [x,y])

    return => list of lists [...,[xi,xj],...]

    '''

    myDist=[]

    for i in range(len(centers)):        
        myDist.append(np.random.normal(mu, sigma, size=(n_points,2)) + centers[i]*np.ones((n_points,2)))

    #print(len(myDist)*n_points)

    return myDist


#function that calculates the mean of our ditributions (classes) 
def means(dist):
    '''
    this function calculate the mean of a distribution 

    dist : your distribution (typr : list of lists [...,[xi,xj],...])    

    return => meanX(float) , meanY(float)
    '''
    lenghtDist=len(dist)
    meanX=0
    meanY=0
    for i in range(lenghtDist):
        meanX += dist[i][0]
        meanY += dist[i][1]
    meanX = (1/lenghtDist)*meanX 
    meanY = (1/lenghtDist)*meanY
    return meanX, meanY

#function that calculates the variance of our ditributions (classes) 
def covars(dist):
    '''
    this function calculate the cavariance of a distribution 

    dist : your distribution (typr : list of lists [...,[xi,xj],...])
    
    return => covX(float) , covY(float)
    
    '''
    lenghtDist=len(dist)
    meanX = means(dist)[0]
    meanY = means(dist)[1]

    covX=0
    covY=0
    for i in range(lenghtDist):
        covX += (dist[i][0]-meanX)**2
        covY += (dist[i][1]-meanY)**2
    
    covX = (1/lenghtDist)*covX
    covY = (1/lenghtDist)*covY

    return covX , covY

#function that calculate the estimation of appartance of a point to a distribution (classe)
def ClasseConditionalDensity(myDist,listReader,mean,cov):
    '''
    this function calculate an estimation for a point to be in a distribution (to use in a loop -> list reader)

    myDist      : a point [x,y]    
    listReader  : a fariable that take the role of an iterator
    mean        : the mean of a distribution
    cov         : the cov of a distribution 
    
    mean and cov should be of the same distribution !
    
    return => classe conditional density (float)
    
    '''

    meanX= mean[0]
    meanY= mean[1]

    covX= cov[0]
    covY= cov[1]

    cdpX= (1/covX*(mt.sqrt(2*mt.pi))*mt.exp(-0.5*((myDist[listReader][0]-meanX)/covX)**2))
    cdpY= (1/covY*(mt.sqrt(2*mt.pi))*mt.exp(-0.5*((myDist[listReader][1]-meanY)/covY)**2))

    return cdpX * cdpY

#fuction that seperate the ploting space to seperate spaces of seperate classes 
#all the blue space is for classe 1 and the red pace is for classe 2
def spaceClasseShow(myDist,dist,mean1,cov1,mean2,cov2):

    '''
    this function seperats in different colors the space that  respects the carracteristiques of our distributions

    myDist      : are a list of points that would cover all the space
    
                    example :   points=[]
                                for i in np.arange(-3,4,0.2):
                                    for j in np.arange(-3, 4, 0.2):
                                        points.append([i,j])

    dist        : your distribution
    mean1,cov1  : the mean and cov of the fisrt classe 
    mean2,cov2  : the mean and cov of the second classe

    return => plot
    '''

    lenghOfMyDist = len(myDist)

    dist1=dist[0]
    dist2=dist[1]

    for i in range(lenghOfMyDist):

        #if ClasseConditionalDensity(myDist,i,mean1,cov1) == ClasseConditionalDensity(myDist,i,mean2,cov2):
            #plt.plot(np.array(myDist)[i, 0], np.array(myDist)[i, 1], c='purple')

        if ClasseConditionalDensity(myDist,i,mean1,cov1) > ClasseConditionalDensity(myDist,i,mean2,cov2):

            plt.scatter(np.array(myDist)[i, 0], np.array(myDist)[i, 1], c='c',  marker='o', s=5)

        else :

            plt.scatter(np.array(myDist)[i, 0], np.array(myDist)[i, 1], c='pink',  marker='o', s=5)

        

#function that plots our ditribution
def clusterPlot(dists):
    '''
    this fuction plots your distributions

    dist : your distribution
    
    '''

    dist1 , dist2 = dists[0] ,  dists[1]

    plt.scatter(dist1[:, 0], dist1[:, 1], c='b',  marker='+', s=35)
    plt.scatter(dist2[:, 0], dist2[:, 1], c='r',  marker='x', s=35)   





#initilising our distributions
mu=0
sigma=0.5
n_points = 25
centers=[[1,1],[-1,-1]]

#generating our distributions
dist = randomDataGenerator(mu, sigma, n_points, centers)

dist1 = dist[0]
dist2 = dist[1]

#computing the means and the vars of our distributions
mean1=means(dist1)
covs1=covars(dist1)

mean2=means(dist2)
covs2=covars(dist2)


#generating point that would cover all the space, used to show our space separation
points=[]
for i in np.arange(-3,4,0.2):
    for j in np.arange(-3, 4, 0.2):
        points.append([i,j])

#ceparating the space
spaceClasseShow(points,dist,mean1,covs1,mean2,covs2)

#ploting
clusterPlot(dist)

plt.show()


